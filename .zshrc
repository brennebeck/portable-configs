# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

ZSH_THEME="agnoster-mod"
DEFAULT_USER="brennebeck"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
export UPDATE_ZSH_DAYS=15

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git ruby bower cabal composer cap coffee docker gem jsontools lein node npm pip pyenv python rvm scala sprunge taskwarrior vagrant web-search zshmarks zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

export PATH=$PATH:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:$HOME/bin


#PATH=$PATH:/usr/local/rvm/bin          # Add RVM to PATH for scripting
#PATH=$PATH:$HOME/.rvm/bin              # Add RVM to PATH for scripting
#PATH=$PATH:$HOME/.composer/vendor/bin  # Add global composer packages


## Alias file?
# source ~/.alias.sh

## AutoJump?
#[[ -s `brew --prefix`/etc/autojump.sh ]] && . `brew --prefix`/etc/autojump.sh

## Get our standard ENV variables
#source ~/.env.sh

## Source `z.sh`
#source ~/Development/z/z.sh

## pyenv?
#if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi

## GVM?
#THIS MUST BE AT THE END OF THE FILE FOR GVM TO WORK!!!
[[ -s "/Users/brennebeck/.gvm/bin/gvm-init.sh" ]] && source "/Users/brennebeck/.gvm/bin/gvm-init.sh"

## RVM?
##[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "/usr/local/rvm/scripts/rvm" # Load RVM into a shell session *as a function*
